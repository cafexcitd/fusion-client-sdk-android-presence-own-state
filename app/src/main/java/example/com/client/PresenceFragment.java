package example.com.client;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.alicecallsbob.fcsdk.android.presence.Conversation;
import com.alicecallsbob.fcsdk.android.presence.Presence;
import com.alicecallsbob.fcsdk.android.presence.PresenceListener;
import com.alicecallsbob.fcsdk.android.presence.PresenceStatus;

/**
 * Created by Nathan on 07/04/2016.
 */
public class PresenceFragment extends Fragment implements PresenceListener {

    private ArrayAdapter<PresenceStatus> statusChoicesAdapter = null;
    private EditText customMessageStatusText = null;
    private Spinner statusSpinner = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_presence, container, false);

        // cache the UI elements we'll need
        this.customMessageStatusText = (EditText) view.findViewById(R.id.statusMessage);
        this.statusSpinner = (Spinner) view.findViewById(R.id.status);

        // populate the status spinner with the available choices
        this.statusChoicesAdapter = new ArrayAdapter<PresenceStatus>(
                this.getActivity(),
                R.layout.support_simple_spinner_dropdown_item,
                PresenceStatus.values());

        this.statusSpinner.setAdapter(this.statusChoicesAdapter);

        // configure the button to update the presence state
        Button updateStatusButton = (Button) view.findViewById(R.id.update);
        updateStatusButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // find the current status of the custom message
                PresenceStatus status = (PresenceStatus) statusSpinner.getSelectedItem();
                String customStatusMessage = customMessageStatusText.getText().toString();

                // log...
                addMessageToLog("Updating state: " + status + ", " + customStatusMessage);

                // now update the users' presence state
                if (ConfigFragment.UC != null) {
                    Presence presence = ConfigFragment.UC.getPresence();
                    presence.setStatus(status, customStatusMessage);
                }
            }
        });


        // return the prepared statement
        return view;
    }

    // adds a message to the log
    private void addMessageToLog(final String message) {
        final TextView textView = (TextView) this.getView().findViewById(R.id.log);
        this.getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                textView.setText(message + "\n" + textView.getText());
            }
        });
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(this.getView().getWindowToken(), 0);
    }



    //////////////////////////////////////
    //
    // PresenceListener

    @Override
    public void onUserStatusChanged(final PresenceStatus presenceStatus, final String customMessage) {
        this.addMessageToLog("onUserStatusChanged: " + presenceStatus + " : " + customMessage);


        this.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // set the value of the status spinner
                int position = statusChoicesAdapter.getPosition(presenceStatus);
                statusSpinner.setSelection(position);

                // set the value of the custom message
                customMessageStatusText.setText(customMessage);
            }
        });
    }

    @Override
    public void onUserStatusChangeFailed(PresenceStatus presenceStatus, String customMessage) {
        this.addMessageToLog("onUserStatusChangeFailed");
    }

    @Override
    public void onContactStatusChanged(String contactAddress, PresenceStatus presenceStatus, String customMessage) {

    }

    @Override
    public void onConversationStarted(Conversation conversation) {

    }
}
